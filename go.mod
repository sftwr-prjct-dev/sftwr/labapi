module gitlab.com/sftwr-prjct-dev/sftwr/labapi

go 1.14

require (
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/rs/cors v1.7.0
	github.com/sirupsen/logrus v1.6.0
)
