package main

import (
	"gitlab.com/sftwr-prjct-dev/sftwr/labapi/src/api"
	"gitlab.com/sftwr-prjct-dev/sftwr/labapi/src/config"
	l "gitlab.com/sftwr-prjct-dev/sftwr/labapi/src/logging"
)

func main() {
	l.InitLog(config.Env.Environment)
	api.Run()
}
