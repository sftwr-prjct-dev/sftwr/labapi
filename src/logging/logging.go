package logging

import (
	"os"

	log "github.com/sirupsen/logrus"
)

const (
	development = "development"
)

func InitLog(env string) {
	if env == development {
		log.SetFormatter(&log.TextFormatter{
			DisableColors: true,
			FullTimestamp: true,
		})
		log.SetOutput(os.Stdout)
	}
	// log.SetFormatter(&log.JSONFormatter{})
	// log.SetLevel(log.WarnLevel)
}
