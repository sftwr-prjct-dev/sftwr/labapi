package config

import (
	"log"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

func init() {
	load()
}

// Config contains the env definition for this project
type Config struct {
	AppName     string `split_words:"true" required:"true"`
	Port        int    `required:"true"`
	Environment string `required:"true"`
}

// Env contains the loaded env for this project
var Env Config

func load() {
	if err := godotenv.Load("src/env/.env"); err != nil {
		log.Println("No .env file found in src/env/")
		log.Println(err)
	}

	if err := envconfig.Process("labapi", &Env); err != nil {
		log.Fatal(err.Error())
	}

}
