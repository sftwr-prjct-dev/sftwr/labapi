package utils

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Response struct {
	Success bool        `json:"success"`
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}

func JSONResponse(w http.ResponseWriter, statusCode int, data Response) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		fmt.Fprintf(w, "%s", err.Error())
	}
}

func ERRORResponse(w http.ResponseWriter, statusCode int, err error) {
	if err != nil {
		JSONResponse(w, statusCode, Response{
			Success: false,
			Message: err.Error(),
		})
		return
	}
	JSONResponse(w, http.StatusBadRequest, Response{false, "Bad request", nil})
}
