package controllers

import (
	"net/http"

	"gitlab.com/sftwr-prjct-dev/sftwr/labapi/src/api/utils"
)

// Home ...
func Home(w http.ResponseWriter, r *http.Request) {
	utils.JSONResponse(w, http.StatusOK, utils.Response{
		Success: true,
		Message: "LabApi: Welcome!",
		Data:    nil,
	})
}

// Ping ...
func Ping(w http.ResponseWriter, r *http.Request) {
	utils.JSONResponse(w, http.StatusOK, utils.Response{Success: true, Message: "Pong!", Data: nil})
}
