package api

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	log "github.com/sirupsen/logrus"
	"gitlab.com/sftwr-prjct-dev/sftwr/labapi/src/api/middlewares"
	"gitlab.com/sftwr-prjct-dev/sftwr/labapi/src/api/router"
	"gitlab.com/sftwr-prjct-dev/sftwr/labapi/src/config"
)

func Run() {
	listen(config.Env.Port)
}

func listen(port int) {
	r := router.New()
	r.Use(middlewares.ResponseHeaderMiddleware)
	corsOpts := handlers.AllowedOrigins([]string{"*"})
	loggedRouter := handlers.LoggingHandler(os.Stdout, r)
	log.Infof("Application [ %s ] started on port [ %d ]", config.Env.AppName, config.Env.Port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), handlers.CORS(corsOpts)(loggedRouter)))
}
