package routes

import (
	"net/http"

	"github.com/gorilla/mux"
)

// Route ...
type Route struct {
	URL     string
	Method  string
	Handler func(http.ResponseWriter, *http.Request)
}

// Load ...
func load() []Route {
	routes := []Route{}
	routes = append(routes, homeRoutes...)
	return routes
}

// SetupRoutes ...
func SetupRoutes(r *mux.Router) *mux.Router {
	for _, route := range load() {
		r.HandleFunc(route.URL, route.Handler).Methods(route.Method)
	}
	return r
}
