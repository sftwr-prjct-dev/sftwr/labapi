package routes

import (
	"net/http"

	"gitlab.com/sftwr-prjct-dev/sftwr/labapi/src/api/controllers"
)

var homeRoutes = []Route{
	Route{
		URL:     "/",
		Method:  http.MethodGet,
		Handler: controllers.Home,
	},
	Route{
		URL:     "/ping",
		Method:  http.MethodGet,
		Handler: controllers.Ping,
	},
}
