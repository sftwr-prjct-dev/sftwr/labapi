package router

import (
	"github.com/gorilla/mux"
	"gitlab.com/sftwr-prjct-dev/sftwr/labapi/src/api/router/routes"
)

// New ...
func New() *mux.Router {
	r := mux.NewRouter().StrictSlash(true)
	return routes.SetupRoutes(r)
}
